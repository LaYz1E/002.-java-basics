package Courses;

import java.util.ArrayList;

/**
 * Created by Dmitrii_Goi on 6/23/2017.
 */
public class Course{

    private CourseEnum course;

    public CourseEnum getCourse(){
        return course;
    }
    public void setCourse(CourseEnum courseSet){
        this.course=courseSet;
    }

    public Course(CourseEnum courseEnum){
        this.course=courseEnum;
    }

    public int getLength(){
        return course.getLength();
    }

}

