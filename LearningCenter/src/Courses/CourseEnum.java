package Courses;

/**
 * Created by Dmitrii_Goi on 6/23/2017.
 */
public enum CourseEnum {

    JavaServlets(16), StrutsFramework(8), SpringFramework(4), Hibernate(4),
    JavaCore(32), JFCSwing(8), JDBC(8), JAX(4), Commons(4), JavaUtil(8),
    JavaScript(16), dotNet(64), html(16), css(16), SQl(8);

   private final int LENGTH;

   CourseEnum(int len){
       this.LENGTH=len;

   }
   public int getLength(){
       return LENGTH;
   }
}