package Curriculums;

import Courses.Course;
import Courses.CourseEnum;
import java.util.ArrayList;

/**
 * Created by Dmitrii_Goi on 6/23/2017.
 */
public class Curriculum {
    private CurriculumEnum curriculum;
    private ArrayList<Course> courses= new ArrayList<>();

    public CurriculumEnum getCurrriculum(){
        return curriculum;
    }

    public void setCurriculum(CurriculumEnum curriculumEnum){
        System.out.println(curriculumEnum);
        this.curriculum=curriculumEnum;
    }

    public int learningTime(){
        int time=0;
        for (Course course: courses) {
            time+=course.getLength();
        }
        return time;
    }

    public Curriculum(){

    }

    @Override
    public String toString() {
        return " " + curriculum;
    }

    public Curriculum(CurriculumEnum curEnum){
        curriculum = curEnum;
        this.courses = this.getCourses();
    }

    public ArrayList<Course> getCourses(){
        courses = new ArrayList<Course>();
        switch (curriculum){
            case Frontend:
                courses.add(new Course(CourseEnum.html));
                courses.add(new Course(CourseEnum.css));
                courses.add(new Course(CourseEnum.JavaScript));
                break;
            case JavaBackend:
                courses.add(new Course(CourseEnum.JavaCore));
                courses.add(new Course(CourseEnum.JavaServlets));
                courses.add(new Course(CourseEnum.JavaUtil));
                courses.add(new Course(CourseEnum.JAX));
                courses.add(new Course(CourseEnum.SpringFramework));
                courses.add(new Course(CourseEnum.Hibernate));
                courses.add(new Course(CourseEnum.StrutsFramework));
                courses.add(new Course(CourseEnum.Commons));
                courses.add(new Course(CourseEnum.JDBC));
                courses.add(new Course(CourseEnum.JFCSwing));
                break;
            case DotNetBackend:
                courses.add(new Course(CourseEnum.dotNet));
                break;
            case DataBaseArchitector:
                courses.add(new Course(CourseEnum.SQl));
                break;
        }
        return courses;
    }

}
