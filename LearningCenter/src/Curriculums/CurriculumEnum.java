package Curriculums;

import Courses.Course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static Courses.Course.*;
import static Courses.CourseEnum.*;

/**
 * Created by Dmitrii_Goi on 6/23/2017.
 */
public enum CurriculumEnum {
    JavaBackend,
    Frontend,
    DotNetBackend,
    DataBaseArchitector

}
