package Student;

import Curriculums.CurriculumEnum;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Dmitrii_Goi on 6/23/2017.
 */
public class WorkFlow {
    List<Student> students = new ArrayList<Student>();
   public  WorkFlow() {
        students.add(new Student("Vasya", CurriculumEnum.JavaBackend, LocalDateTime.of(2017, Month.JUNE, 20, 12, 00)));
        students.add(new Student("Petya", CurriculumEnum.Frontend, LocalDateTime.of(2017, Month.JUNE, 21, 11, 30)));
        students.add(new Student("Sasha", CurriculumEnum.DataBaseArchitector, LocalDateTime.of(2017, Month.JUNE, 22, 8, 30)));
        students.add(new Student("Nikita", CurriculumEnum.DotNetBackend, LocalDateTime.of(2017, Month.JUNE, 19, 11, 30)));
        students.get(0).addMark(5).addMark(4).addMark(5).addMark(5).addMark(4);
        students.get(1).addMark(4).addMark(3).addMark(2).addMark(4).addMark(3);
        students.get(2).addMark(4).addMark(4).addMark(5).addMark(4).addMark(4);
        students.get(3).addMark(4).addMark(4).addMark(4).addMark(4).addMark(5);
    }

    public static class StudentAvgMarkComparator implements Serializable, Comparator<Student> {
        public int compare(Student st1, Student st2) {
            if(st1.getAverageMark()>st2.getAverageMark()) return 1;
            else return -1;
        }
    }

    public static class StudentRemainingLearningTimeComparator  implements Serializable, Comparator<Student> {
        public int compare(Student st1, Student st2) {
            if (st1.learninTime() > st2.learninTime()) return 1;
            else return -1;
        }
    }

    public void sortByAvgMark(){
       students.sort(new StudentAvgMarkComparator());
    }

    public void sortByRemainingLearningTime(){
            students.sort(new StudentRemainingLearningTimeComparator());
    }

    public void out(){
        System.out.println(students);
    }

    public void dangerousStudents(){
        for (Student student: students) {
            if(!student.potentialEmployee()){
                System.out.println(student.getName()+ " have potential average mark " + student.potentialAvgMark()+ " may be expelled");
            }

        }
    }

    public void potentialEmployee(){
        for (Student student: students) {
            if(student.potentialEmployee()){
                System.out.println(student.getName()+ " have potential average mark " + student.potentialAvgMark()+ " may be recruited");
            }

        }
    }
}
