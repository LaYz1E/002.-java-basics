package Student;

import Curriculums.Curriculum;
import Curriculums.CurriculumEnum;

import java.util.ArrayList;
import java.time.*;

/**
 * Created by Dmitrii_Goi on 6/23/2017.
 */
public class Student {
    private Curriculum curriculum= new Curriculum();
    private String name;
    private ArrayList<Integer> marks = new ArrayList<>();
    private LocalDateTime startDate;

    public Student(String name, CurriculumEnum curriculumEnum,LocalDateTime startDate){
        curriculum.setCurriculum(curriculumEnum);
        this.name = name;
        this.startDate=startDate;

    }

    public void insertMarks(ArrayList<Integer> newMarkList){
        marks=newMarkList;
    }

    public Student addMark(Integer mark){
        marks.add(mark);
        return this;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long learninTime(){
        return Duration.between(startDate,LocalDateTime.now()).toDays();
    }

    public long remainingLearningTime(){
        return learninTime()- (long) curriculum.learningTime();
    }

    public float getAverageMark(){
        float avgMark = 0;
        int num=0;
        for (int item: marks){
            ++num;
            avgMark+=(float)item;
        }
        //avgMark/=num;
        return avgMark/num;

    }

    public boolean potentialEmployee(){

        float sumMark = 0;
        int num=0;
        for (int item: marks) {
            ++num;
            sumMark += (float) item;
        }
        return (sumMark + remainingLearningTime() * 5) / (num + remainingLearningTime()) >= 4.5;
    }

    public float potentialAvgMark(){

        float sumMark = 0;
        int num=0;
        for (int item: marks) {
            ++num;
            sumMark += (float) item;
        }
        return (sumMark+remainingLearningTime()*5)/(num+remainingLearningTime());
    }

    @Override
    public String toString() {
        return "\n {" +
                name + " " + curriculum +
                ", average mark=" + getAverageMark()+
                ", marks=" + marks +
                ", startDate=" + startDate +
                ", remaining learning time=" + learninTime() +
                "}";
    }

}
